# ngTeam #

* A simple team maker written with AngularJS and Bootstrap 3
* Version 1.0

### Run ###

* Open ngteam.html in a browser

### Development ###

* All app-specific code lives in the following files
    * ngTeam.html
    * /js/ngTeam.js
    * /css/ngTeam.css