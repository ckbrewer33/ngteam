var ngTeam = angular.module('ngTeam', []);

ngTeam.factory('teamService', [function() {
	return {
		shufflePlayers: function(players) {
			var counter = players.length,
				temp,
				index;

			// While there are elements in the array
			while (counter > 0) {
				// Pick a random index
				index = Math.floor(Math.random() * counter);

				// Decrease counter by 1
				counter--;

				// And swap the last element with it
				temp = players[counter];
				players[counter] = players[index];
				players[index] = temp;
			}

			return players;
		},

		createTeams: function(players, numTeams) {
			var teams = [],
				teamCount = 0,
				i;

			// Fill the teams array with empty arrays for teams
			for (i = 0; i < numTeams; ++i)
			{
				teams[i] = [];
			}

			// Divide players into teams
			i = 0;
			angular.forEach(players, function(player) {
				if (teamCount === numTeams)
				{
					teamCount = 0;
					++i;
				}
				teams[teamCount][i] = player;
				++teamCount;
			});

			return teams;
		}
	}
}]);

ngTeam.controller('MainCtrl', ['$scope', 'teamService', function($scope, teamService) {
    $scope.players = [
        {name: ''},
        {name: ''},
        {name: ''},
        {name: ''},
        {name: ''},
        {name: ''},
        {name: ''},
        {name: ''},
        {name: ''},
        {name: ''},
        {name: ''},
        {name: ''}
    ];
	$scope.mixedPlayers = [];
	$scope.teams;
	$scope.numTeams = "2";

	$scope.generateTeams = function() {
		$scope.mixedPlayers = [];

		// Add only player input's with names entered to list of players
		angular.forEach($scope.players, function(player) {
			if ('' !== player.name.trim())
			{
				$scope.mixedPlayers.push(angular.copy(player));
			}
		});

		$scope.mixedPlayers = teamService.shufflePlayers($scope.mixedPlayers);
		$scope.teams = teamService.createTeams($scope.mixedPlayers, parseInt($scope.numTeams));
    }
}]);